# Sampler

An `Arduino` compatible sampler for data acquisition. Different from the
existing `analogRead` function to perform single sampling, this library is
meant for continuous data acquisition. It can be configured though to handle
`one time` sampling.

