/*
  The present example configure the sampling in auto trigger mode
  
  how it works
  ------------
  A serie of measurements are done and save within the buffer provided. Then
  the measurements stop until the next samlping is requested. This is done
  through the `next` method of the sampler.
 */

#include <sampler.h>

#define BUFFER_SIZE 300
#define SAMPLING_FREQUENCY 5 // Hz

// enable auto trigger
// 10-bit resolution sampling
Sampler adc(true, SAMPLER_10_BIT);
uint16_t buf[BUFFER_SIZE];

void setup(){
  Serial.begin(9600);
  delay(100);
  // init the sampler
  adc.begin(SAMPLING_FREQUENCY);
  // select channel 0
  adc.selectChannel(0);
  // configure the buffer
  adc.fillBuffer(buf, BUFFER_SIZE);
  // start acquisition
  adc.start();
  // wait until the buffer is filled
  while(!adc.isBufferReady()){ delay(10); }
}

void loop(){
  if(adc.isBufferReady()){
    for(uint16_t i = 0; i < BUFFER_SIZE; i++){
      Serial.println(buf[i] * 5.0/1024.0);
      // Serial.flush();
    }
    // request new samples
    // the buffer will be filled one more time
    adc.next();
  }
}
