#include <sampler.h>
#define SAMPLING_FREQUENCY 5 // Hz
#define CHANNEL_0 0
#define CHANNEL_1 1
Sampler adc(false, SAMPLER_10_BIT);

// channel one value
volatile uint16_t ch_one_value = 0;

// channel two value
volatile uint16_t ch_two_value = 0;

volatile unsigned char channel_control = 0;

void setup() {
  Serial.begin(9600);
  delay(100);
  adc.begin(SAMPLING_FREQUENCY);
  adc.selectChannel(CHANNEL_0);
  adc.attachInterrupt(getter);
  adc.start();
}

void loop() {
  Serial.print(ch_one_value);
  Serial.print(", ");
  Serial.println(ch_two_value);
  delay(10);
}

void getter() {
  // get new sample
  if (channel_control == 0) {
    ch_one_value = adc.getSample();
  } else if (channel_control == 1) {
    ch_two_value = adc.getSample();
  }
  // switch channel
  if (channel_control == 0) {
    channel_control = 1;
    adc.selectChannel(CHANNEL_1);
  } else {
    channel_control = 0;
    adc.selectChannel(CHANNEL_0);
  }
  // start next sampling
  adc.start();
}

