#include <sampler.h>
#define SAMPLING_FREQUENCY 5 // Hz
Sampler adc(false, SAMPLER_10_BIT);

volatile uint16_t value = 0;

void setup() {
  Serial.begin(9600);
  delay(100);
  adc.begin(SAMPLING_FREQUENCY);
  // select ADC channel
  adc.selectChannel(0);
  // attach an interrupt handler
  adc.attachInterrupt(getter);
  adc.start();
}

void loop() {
  Serial.println(value);
  delay(100);
}

/**
 * @function: getter
 * 
 * \brief This function is run every time an ADC interrupt occur.
 */
void getter(){
  // read raw adc sample
  value = adc.getSample();
  // start the next ADC
  adc.start();
}
