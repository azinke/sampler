/*
  Continuous acquisition on mutiple channels

  ----
  Testing channel switching in continuous acquisition case
*/
#include <sampler.h>
#define BUFFER_SIZE 200
#define SAMPLING_FREQUENCY 10000 // Hz
#define CHANNEL_0 0 // A0 pin
#define CHANNEL_1 1 // A1 pin
Sampler adc(true, SAMPLER_10_BIT);
uint16_t buffer[BUFFER_SIZE];
unsigned char channel_control = 0;
unsigned long start_time = 0;

void setup() {
  Serial.begin(9600);
  delay(100);
  // init sampler
  adc.begin(SAMPLING_FREQUENCY);
  // select channel
  adc.selectChannel(CHANNEL_0);
  // configure buffer
  adc.fillBuffer(buffer, BUFFER_SIZE);
  // start acquisition
  adc.start();
  start_time = micros();
  while(!adc.isBufferReady()){
    delayMicroseconds(1);
  }
  Serial.print("Time to fill the buffer: ");
  Serial.print(micros() - start_time);
  Serial.println(" us");
  Serial.flush();
}

void loop() {
  if (adc.isBufferReady()) {
    for (uint16_t i = 0; i < BUFFER_SIZE; i++) {
      Serial.println(buffer[i] * 5./1024.);
    }
    if (channel_control == 0) {
      channel_control = 1;
      adc.selectChannel(CHANNEL_1);
    } else {
      channel_control = 0;
      adc.selectChannel(CHANNEL_0);
    }
    adc.next();
  }
}
