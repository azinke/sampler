/**
 * @author: AMOUSSOU Z. Kenneth
 * @date: 18-08-2019
 * @version: 1.0
 * @compatibility: [
 *    Arduino uno
 * ]
 * @summary: library to perform precise data acquisition tasks
 */

#ifndef H_SAMPLER
#define H_SAMPLER

# if ARDUINO < 100
  #include <WProgram.h>
#else
  #include <Arduino.h>
#endif

// conversion mode
#define SAMPLER_FREE_RUNNING_MODE   0
#define SAMPLER_ANALOG_COMP_MODE    1
#define SAMPLER_EXT_INT_MODE        2
#define SAMPLER_TIMER0_COMP         3
#define SAMPLER_TIMER0_OVF          4
#define SAMPLER_TIMER1_COMP         5
#define SAMPLER_TIMER1_OVF          6
#define SAMPLER_TIMER1_EVENT        7  // capture event

// internal adc reference voltage
#define SAMPLER_INTERNAL_VREF_DISABLE   0 // internal vref disabled
#define SAMPLER_VREF_VCC                1 // internal vref linked to VCC
#define SAMPLER_VREF_DEFAULT            3  // internal vref = 1.1v

// sampler's resolution
#define SAMPLER_8_BIT    8
#define SAMPLER_10_BIT  10

// ADC frequency configuration
#define SAMPLER_FREQ_2      0
#define SAMPLER_FREQ_4      2
#define SAMPLER_FREQ_8      3
#define SAMPLER_FREQ_16     4
#define SAMPLER_FREQ_32     5
#define SAMPLER_FREQ_64     6
#define SAMPLER_FREQ_128    7

/** Static variables definitions */
static volatile uint16_t lastRawSample;

static volatile uint8_t* buffer8 = NULL;  // 8-bit size buffer
static volatile uint16_t* buffer16 = NULL; // 16-bit size buffer
static volatile bool fill_buffer = false;  // buffer status
static volatile uint16_t buffer_size = 0;
static volatile uint16_t buffer_index = 0;
static volatile nop(){ asm("nop");  };
static volatile void (*callback)(void) = nop;


class Sampler{
  public:
    Sampler(bool auto_trigger, uint8_t resolution=10);
    
    /**
     * Configure the sampler
     */
    void begin(uint32_t sampling_freq);

    /**
     * start the sampling in continuous acquisition mode
     */
    void start();

    /**
     * select the channel to sample in continuous acquisition mode
     */
    void selectChannel(uint8_t input);

    /**
     * read the next output of the sampler in continuous acquisition mode
     */
    void next();

    /**
     * set sampling frequency in Hertz
     *
     * @note: this is based on timer/counter 1 for the uC
     */
    void setSamplingFrequency(uint32_t freq);

    /**
     * fill 8-bit sized buffer
     */
    void fillBuffer(uint8_t* buf, uint16_t size);
    
    /**
     * fill 16-bit sized buffer
     */
    void fillBuffer(uint16_t* buf, uint16_t size);

    /**
     * check the filled-in state of the buffer
     */
    bool isBufferReady();

    /**
     * get the internal buffer to NULL
     */
    bool detachBuffer();

    /**
     * read ADC result
     */
    float read(uint8_t input);
    
    /**
     * \brief Get the last sample of ADC.
     * It's recommended to use this method inside ADC ISR.
     *
     */
    volatile uint16_t getSample();
 
    /**
     * set analog reference voltage
     */
    void setVref(float value);

    /**
     * set internal vref
     */
    void setInternalVref(uint8_t value);

    /**
     * read the analof reference voltage
     */
    float getVref();

    /**
     * set conversion mode
     */
    void setMode(uint8_t mode);

    /**
     * set sampler resolution
     */
    void setResolution(uint8_t resolution);

    /**
     *  set ADC frequency prescaler
     */
    void setADCPrescaler(uint8_t prescaler);

    /**
     * stop sampler. Disable ADC module
     */
    void stop();

    /**
     * resume sampler.A Enable the ADC module
     */
    void resume();

    /**
     * set the uC frequency in kilo-hertz (KHz)
     */
    inline void setCPUFrequency(uint16_t freq);
    
    /**
     * attach a callback to the sampler and fun it every time an ADC interrupt
     * occur.
     */
    void attachInterrupt(void (*func)(void));

    /**
     * disable ADC module interrupt when not needed
     */
    void disableInterrupt();
  
    /**
     * enable ADC module interrupt 
     */
    void enableInterrupt();

  private:
      float _raw_data;
    /**
     * Reference voltage used for the analog digital converter
     */
    float _v_ref;
    /**
     * Sampler resolution
     */
    uint8_t _resolution;
    /**
     * microcontroller's frequency in KHz
     */
    uint32_t _f_cpu;
    /**
     * ADC conversion mode
     */
    uint8_t _adc_mode;
    /**
     * ADC auto trigger
     */
    bool _auto_trigger;

    // Private methods
    
    /**
     * configure the timer/counter 1
     */
  void _configureTimer1(uint32_t sampling_freq);
    
};

#endif
