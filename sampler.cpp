/**
 * @author AMOUSSOU Z. Kenneth
 * @date 18-08-2019
 * @version 1.0
 */
#include "sampler.h"

/**
 * \brief Class constructor 
 *
 * @param auto_trigger: config triggering mode
 * @param mode: ADC conversion mode configuration
 */
Sampler::Sampler(bool auto_trigger, uint8_t resolution=10){
  // F_CPU: 16MHz
  _f_cpu = 16000000;
  _adc_mode = SAMPLER_TIMER1_COMP;
  _auto_trigger = auto_trigger;
  //default:  10-bit resolution ADC
  _resolution = resolution;
}

/**
 * \brief Configure the timer/counter 1
 * @param sampling_freq: Sampling frequency to configure the timer
 */
void Sampler::_configureTimer1(uint32_t sampling_freq){
  TCCR1A = 0x00; // _BV(WGM11) | _BV(WGM10);
  // no prescaler by default
  TCCR1B = _BV(WGM12) | _BV(CS10); // | _BV(WGM13) ;
  // samplimg frequency configure to 10kHz by default
  setSamplingFrequency(sampling_freq);
  // interrupt flag
  // Writing one to this bit location clear the interrupt flag
  TIFR1 |= _BV(OCF1B);
  TCNT1 = 0x00;
  TIMSK1 |= _BV(OCIE1B);
}

/**
 * \brief Set the sampling frequency
 *
 * @param freq: sampling frequency in Hertz
 */
void Sampler::setSamplingFrequency(uint32_t freq){
  uint16_t load = _f_cpu / freq;
  // Output compare A register
  OCR1A = load;
  // Output compare B register
  OCR1B = load;
}

/**
 * \brief Sampler initialization. Configure the ADC registers for the sampler
 *        to be ready to work
 *  @param sampling_freq: Sampling frequency in hertz (Hz)
 */
void Sampler::begin(uint32_t sampling_freq){
  cli();
  // config the sampler's resolution
  setResolution(_resolution);
  if (_auto_trigger) {
    ADCSRA |= _BV(ADATE);
    // select Timer1 Output compare B as triggering source
    setMode(_adc_mode);
    _configureTimer1(sampling_freq);
  } else ADCSRA &= !_BV(ADATE);
  // Sampling frequency: 125kHz
  setADCPrescaler(SAMPLER_FREQ_128);
  setInternalVref(SAMPLER_VREF_VCC);
  ADCSRA |= _BV(ADIF); // clear ADC interrupt flag
  ADCSRA |= _BV(ADIE); // enable ADC interruptions
  ADCSRA |= _BV(ADEN);
  sei(); // global interrupt enable
}

/**
 * \brief Stop the sampler. Disable ADC module
 */
void Sampler::stop(){
  ADCSRA &= !_BV(ADEN);
}

/**
 * \brief Resume the sampler. Enable the ADC module
 */
void Sampler::resume(){
  ADCSRA |= _BV(ADEN);
}

/**
 * \brief Set the reference voltage
 *
 * @param value: reference voltage value (used when reference voltage is
 *               configured from AREF)
 */
void Sampler::setVref(float value){
  _v_ref = value;
}

/**
 * \brief Read the configured analog reference voltage
 */
float Sampler::getVref(){
  return _v_ref;
}

/**
 * \brief Set uC internal ADC reference voltage
 *
 * @param value: internal Vref config
 */
void Sampler::setInternalVref(uint8_t value){
  if ((value == 2) || (value > 3)) return;
  ADMUX &= 0x3F;
  ADMUX |= (value << 6);
  if (value == SAMPLER_VREF_VCC) _v_ref = 5.0;
  if (value == SAMPLER_VREF_DEFAULT) _v_ref = 1.1;
}

/**
 * \brief Set sampler resolution
 *
 * @param resolution: sampler's resolution (8-bit or 10-bit)
 */
void Sampler::setResolution(uint8_t resolution){
  // left adjust ADC result
  if (resolution == SAMPLER_8_BIT) {
    ADMUX |= _BV(ADLAR);  
    _resolution = SAMPLER_8_BIT;
  }

  // right adjust the ADC result
  if (resolution == SAMPLER_10_BIT) {
    ADMUX &= !_BV(ADLAR);
    _resolution = SAMPLER_10_BIT;
  }
}

/**
 * \brief Set the ADC mode
 * @param mode: adc mode
 *
 * ADC mode could be one of the following
 * - free running
 * - based on timer
 * - based on external interrupt
 */
void Sampler::setMode(uint8_t mode){
  if (mode > 7) return;
  ADCSRB &= 0xF0;
  ADCSRB |= mode;
}

/**
 * \brief set CPU frequency in kilo hertz
 *
 * @param freq: set the cpu frequency
 */
inline void Sampler::setCPUFrequency(uint16_t freq){
  _f_cpu = freq;
}

/**
 * \brief set the ADC prescaler
 *
 * @param prescaler: prescaler config value
 */
void Sampler::setADCPrescaler(uint8_t prescaler){
  if (prescaler > 7) return;
  ADCSRA &= 0xF8;
  ADCSRA |= prescaler;
}

/**
 * \brief Read ADC conversion's result
 *
 * The result is converted in the range of 0v to 5v
 *
 * @param input: analog input
 */
float Sampler::read(uint8_t input){
  selectChannel(input);
  if(!_auto_trigger){
    ADCSRA |= _BV(ADSC);
    // free running mode
    // wait the conversion to finish
    // convertion time is 25 cycles for the first conversion
    // 13 cycles for the followings
    while(ADCSRA & _BV(ADSC));
  }
  if (_resolution == SAMPLER_8_BIT) {
    _raw_data = ADCL;
    _raw_data = (ADCH * _v_ref)/256.0;
    // TODO: add filter;
  }
  if (_resolution == SAMPLER_10_BIT) {
    _raw_data = ((ADCL | (ADCH << 8)) * _v_ref)/1024.0;
  }
  return _raw_data;
}

/**
 * \brief Start the sampler in continuous acquisition mode
 */
void Sampler::start(){
  ADCSRA |= _BV(ADSC);
}

/**
 * \brief Select the analog input to sample in continuous mode
 */
void Sampler::selectChannel(uint8_t input){
  if (input > 5) return;
  if (input != (ADMUX & 0x0F)) {
    ADMUX &= 0xF0;
    ADMUX |= input;
    // disable digital input buffer
    /*
    switch(input){
      case 0: { DIDR0 |= _BV(ADC0D); break; }
      case 1: { DIDR0 |= _BV(ADC1D); break; }
      case 2: { DIDR0 |= _BV(ADC2D); break; }
      case 3: { DIDR0 |= _BV(ADC3D); break; }
      case 4: { DIDR0 |= _BV(ADC4D); break; }
      case 5: { DIDR0 |= _BV(ADC5D); break; }
      default: { break; }
    }
    */
  }
}

/**
 * \brief Read the next sample of data in continuous acquisition mode
 */
void Sampler::next(){
  fill_buffer = true;
}

/**
 * \brief Fill an 8-bit size buffer
 */
void Sampler::fillBuffer(uint8_t* buf, uint16_t size){
  fill_buffer = true;
  buffer_size = size;
  buffer8 = buf;
}


/**
 * \brief Return the last raw sample of ADC
 */
volatile uint16_t Sampler::getSample(){
  return lastRawSample;
}

/**
 * \brief Fill an 16-bit size buffer
 */
void Sampler::fillBuffer(uint16_t* buf, uint16_t size){
  fill_buffer = true;
  buffer_size = size;
  buffer16 = buf;
}

/**
 * \brief Check the current state of the buffer
 */
bool Sampler::isBufferReady(){
  if (buffer8 != NULL) {
    return !fill_buffer;
  }
  if (buffer16 != NULL) {
    return !fill_buffer;
  }
  return false;
}

/**
 * \brief Set the internal buffer pointer to null
 */
bool Sampler::detachBuffer(){
  if (buffer8 != NULL) {
    buffer8 = NULL;
    return true;
  }
  if (buffer16 != NULL) {
    buffer16 = NULL;
    return true;
  }
  return false;
}

/**
 * \brief Attach callback function to execute every interruption
 */
void Sampler::attachInterrupt(void (*func)(void)){
  callback = func;
}

/**
 * \brief Enable ADC module interrupt
 */
void Sampler::enableInterrupt(){
  ADCSRA &= !_BV(ADEN); // disable ADC module first
  ADCSRA |= _BV(ADIF);  // clear ADC interrupt flag
  ADCSRA |= _BV(ADIE);  // enable ADC interruptions
  ADCSRA |= _BV(ADEN);  // enable ADC module
}

/**
 * \brief Disable ADC module interrupt
 */
void Sampler::disableInterrupt(){
  ADCSRA |= _BV(ADIF); // clear ADC interrupt flag
  ADCSRA &= !_BV(ADIE); // enable ADC interruptions
}

// ====================== ISR ======================

/**
 * ISR
 *
 * Timer/Counter 1 output compare B event interrupt handler
 */
ISR(TIMER1_COMPB_vect){
  // clear Timer1 interrupt flag
  TIFR1 |= _BV(OCF1B);
}

/**
 * ADC ISR
 *
 * End of conversion interrupt handler
 */

ISR(ADC_vect){
  if (ADMUX & _BV(ADLAR)) {
    lastRawSample = ADCL;
    lastRawSample = ADCH;
  } else {
    lastRawSample = ADCL;
    lastRawSample |= (uint16_t)(ADCH << 8);
  }
  // run callback
  (*callback)();
  if (fill_buffer && (buffer_size != 0)) {
    if (buffer8 != NULL) {
      buffer8[buffer_index] = lastRawSample;
      buffer_index++;
      if (buffer_index == buffer_size) {
        buffer_index = 0;
        fill_buffer = false;
      }
    }
    if (buffer16 != NULL) {
      buffer16[buffer_index] = lastRawSample;
      buffer_index++;
      if (buffer_index == buffer_size) {
        buffer_index = 0;
        fill_buffer = false;
      }
    }
  }
  // clear ADC interrupt flag
  ADCSRA |= _BV(ADIF);
}

